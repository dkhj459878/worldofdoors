﻿     using WorldOfDoors.DAL.Common.Models;

     namespace WorldOfDoors.BL.Common.Managers
{
    public interface IWriteOffManager
    {
        WriteOff GetWriteOff(int id);

        int AddWriteOff(WriteOff category);

        void UpdateWriteOff(WriteOff category);

        void DeleteWriteOff(WriteOff category);

        void DeleteWriteOff(int id);
    }
}