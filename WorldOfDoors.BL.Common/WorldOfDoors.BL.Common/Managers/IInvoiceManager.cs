﻿using WorldOfDoors.DAL.Common.Models;

namespace WorldOfDoors.BL.Common.Managers
{
    public interface IInvoiceManager
    {
        Invoice GetInvoice(int id);

        int AddInvoice(Invoice invoice);

        void UpdateInvoice(Invoice invoice);

        void DeleteInvoice(Invoice invoice);

        void DeleteInvoice(int id);
    }
}
