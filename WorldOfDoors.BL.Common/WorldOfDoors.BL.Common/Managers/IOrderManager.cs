﻿using WorldOfDoors.DAL.Common.Models;

namespace WorldOfDoors.BL.Common.Managers
{
    public interface IOrderManager
    {
        Order GetOrder(int id);

        int AddOrder(Order order);

        void UpdateOrder(Order order);

        void DeleteOrder(Order order);

        void DeleteOrder(int id);
    }
}
