﻿using WorldOfDoors.DAL.Common.Models;

namespace WorldOfDoors.BL.Common.Managers
{
    public interface ISubjectTypeManager
    {
        Subject GetSubjectType(int id);

        int AddSubjectType(Subject subject);

        void UpdateSubjectType(Subject subject);

        void DeleteSubjectType(Subject subject);

        void DeleteSubjectType(int id);
    }
}