﻿using WorldOfDoors.DAL.Common.Models;

namespace WorldOfDoors.BL.Common.Managers
{
    public interface ISetDetailManager
    {
        SetDetail GetSetDetail(int id);

        int AddSetDetail(SetDetail setDetail);

        void UpdateSetDetail(SetDetail setDetail);

        void DeleteSetDetail(SetDetail setDetail);

        void DeleteSetDetail(int id);
    }
}
