﻿using WorldOfDoors.DAL.Common.Models;

namespace WorldOfDoors.BL.Common.Managers
{
    public interface IStockManager
    {
        Stock GetStock(int id);

        int AddStock(Stock stock);

        void UpdateStock(Stock stock);

        void DeleteStock(Stock stock);

        void DeleteStock(int id);
    }
}
