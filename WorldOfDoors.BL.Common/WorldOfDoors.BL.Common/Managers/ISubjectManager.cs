﻿using WorldOfDoors.DAL.Common.Models;

namespace WorldOfDoors.BL.Common.Managers
{
    public interface ISubjectManager
    {
        Subject GetSubject(int id);

        int AddSubject(Subject subject);

        void UpdateSubject(Subject subject);

        void DeleteSubject(Subject subject);

        void DeleteSubject(int id);
    }
}