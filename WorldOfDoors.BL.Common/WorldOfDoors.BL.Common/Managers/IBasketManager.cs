﻿using WorldOfDoors.DAL.Common.Models;

namespace WorldOfDoors.BL.Common.Managers
{
    public interface IBasketManager
    {
        Basket GetBasket(int id);

        int AddBasket(Basket basket);

        void UpdateBasket(Basket basket);

        void DeleteBasket(Basket basket);

        void DeleteBasket(int id);
    }
}
