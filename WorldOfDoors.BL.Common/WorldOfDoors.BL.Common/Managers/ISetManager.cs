﻿using WorldOfDoors.DAL.Common.Models;

namespace WorldOfDoors.BL.Common.Managers
{
    public interface ISetManager
    {
        Set GetSet(int id);

        int AddSet(Set set);

        void UpdateSet(Set set);

        void DeleteSet(Set set);

        void DeleteSet(int id);
    }
}
