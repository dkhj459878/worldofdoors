﻿using WorldOfDoors.DAL.Common.Models;

namespace WorldOfDoors.BL.Common.Managers
{
    public interface IItemManager
    {
        Item GetItem(int id);

        int AddItem(Item item);

        void UpdateItem(Item item);

        void DeleteItem(Item item);

        void DeleteItem(int id);
    }
}
