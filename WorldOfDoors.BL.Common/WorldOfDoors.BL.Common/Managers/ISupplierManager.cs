﻿     using WorldOfDoors.DAL.Common.Models;

     namespace WorldOfDoors.BL.Common.Managers
{
    public interface ISupplierManager
    {
        Supplier GetSupplier(int id);

        int AddSupplier(Supplier category);

        void UpdateSupplier(Supplier category);

        void DeleteSupplier(Supplier category);

        void DeleteSupplier(int id);
    }
}