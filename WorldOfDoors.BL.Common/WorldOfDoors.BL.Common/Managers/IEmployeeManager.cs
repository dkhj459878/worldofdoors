﻿using WorldOfDoors.DAL.Common.Models;

namespace WorldOfDoors.BL.Common.Managers
{
    public interface IEmployeeManager
    {
        Delivery GetEmployee(int id);

        int AddEmployee(Employee employee);

        void UpdateEmployee(Employee employee);

        void DeleteEmployee(Employee employee);

        void DeleteEmployee(int id);
    }
}
