﻿using WorldOfDoors.DAL.Common.Models;

namespace WorldOfDoors.BL.Common.Managers
{
    public interface ICategoryManager
    {
        Category GetCategory(int id);

        int AddCategory(Category category);

        void UpdateCategory(Category category);

        void DeleteCategory(Category category);

        void DeleteCategory(int id);
    }
}
