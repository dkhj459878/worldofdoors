﻿using WorldOfDoors.DAL.Common.Models;

namespace WorldOfDoors.BL.Common.Managers
{
    public interface IDeliveryManager
    {
        Delivery GetDelivery(int id);

        int AddDelivery(Delivery delivery);

        void UpdateDelivery(Delivery delivery);

        void DeleteDelivery(Delivery delivery);

        void DeleteDelivery(int id);
    }
}
