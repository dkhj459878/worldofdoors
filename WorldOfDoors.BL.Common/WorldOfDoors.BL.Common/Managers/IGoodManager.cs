﻿using WorldOfDoors.DAL.Common.Models;

namespace WorldOfDoors.BL.Common.Managers
{
    public interface IGoodManager
    {
        Good GetGood(int id);

        int AddGood(Good good);

        void UpdateGood(Good good);

        void DeleteGood(Good good);

        void DeleteGood(int id);
    }
}
