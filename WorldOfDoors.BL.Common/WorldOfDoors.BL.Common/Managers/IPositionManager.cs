﻿using WorldOfDoors.DAL.Common.Models;

namespace WorldOfDoors.BL.Common.Managers
{
    public interface IPositionManager
    {
        Position GetPosition(int id);

        int AddPosition(Position position);

        void UpdatePosition(Position position);

        void DeletePosition(Position position);

        void DeletePosition(int id);
    }
}
