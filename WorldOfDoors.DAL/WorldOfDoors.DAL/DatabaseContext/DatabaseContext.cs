﻿using Microsoft.EntityFrameworkCore;
using WorldOfDoors.DAL.Common.Interfaces.DatabaseContexts;
using WorldOfDoors.DAL.Common.Models;

namespace WorldOfDoors.DAL.DatabaseContext
{
    public class DatabaseContext : DbContext, IDatabaseContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(
                @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=WOfD;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            base.OnConfiguring(optionsBuilder);
        }

        public DbSet<Basket> Baskets { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<ContactInfo> ContactInfos { get; set; }

        public DbSet<Delivery> Deliveries { get; set; }
        public DbSet<Employee> Employees { get; set; }

        public DbSet<Good> Goods { get; set; }

        public DbSet<Invoice> Invoices { get; set; }

        public DbSet<Item> Items { get; set; }

        public DbSet<Order> Orders { get; set; }
        public DbSet<Position> Positions { get; set; }
        public DbSet<Set> Sets { get; set; }
        public DbSet<SetDetail> SetDetails { get; set; }
        public DbSet<Stock> Stocks { get; set; }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<SubjectType> SubjectTypes { get; set; }

        public DbSet<Supplier> Suppliers { get; set; }
        public DbSet<WriteOff> WriteOffs { get; set; }
    }
}