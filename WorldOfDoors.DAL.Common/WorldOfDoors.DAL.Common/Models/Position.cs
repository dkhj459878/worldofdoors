﻿using System.Collections.Generic;
using WorldOfDoors.DAL.Common.Interfaces.Models;

namespace WorldOfDoors.DAL.Common.Models
{
    public class Position : ModelBase
    {
        public IEnumerable<Employee> Employees { get; set; }
    }
}