﻿using System.Linq;
using WorldOfDoors.DAL.Common.Interfaces.Models;

namespace WorldOfDoors.DAL.Common.Models
{
    public class Subject : ModelBase
    {
        public override string Name => GetFullName();

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FartherName { get; set; }

        public int SubjectTypeId { get; set; }

        public SubjectType SubjectType { get; set; }

        public int ContactInfoId { get; set; }

        public ContactInfo ContactInfo { get; set; }

        private string GetFullName()
        {
            var fartherName = string.IsNullOrEmpty(FartherName) ? string.Empty : " ".Concat(FartherName);
            return $"{FirstName} {LastName}{fartherName}";
        }
    }
}