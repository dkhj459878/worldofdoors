﻿using System.Collections.Generic;
using WorldOfDoors.DAL.Common.Interfaces.Models;

namespace WorldOfDoors.DAL.Common.Models
{
    public class SubjectType : ModelBase
    {
        private IEnumerable<Subject> Subjects { get; set; }
    }
}