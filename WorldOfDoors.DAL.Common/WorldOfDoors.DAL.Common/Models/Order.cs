﻿using System.Collections.Generic;
using WorldOfDoors.DAL.Common.Interfaces.Models;

namespace WorldOfDoors.DAL.Common.Models
{
    public class Order : DocumentBase
    {
        public int CustomerId { get; set; }

        public Subject Subject { get; set; }

        public List<Item> Items { get; set; }
    }
}
