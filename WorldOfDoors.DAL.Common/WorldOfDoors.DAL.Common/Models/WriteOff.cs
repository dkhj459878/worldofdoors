﻿using WorldOfDoors.DAL.Common.Enum;
using WorldOfDoors.DAL.Common.Interfaces.Models;

namespace WorldOfDoors.DAL.Common.Models
{
    public class WriteOff : ModelBase
    {
        public int ItemId { get; set; }

        public Item Item { get; set; }

        public int BasketId { get; set; }

        public Basket Basket { get; set; }

        public int DeliveryId { get; set; }

        public Delivery Delivery { get; set; }

        public int Quantity { get; set; }

        public WriteOffStatus Status { get; set; }
    }
}