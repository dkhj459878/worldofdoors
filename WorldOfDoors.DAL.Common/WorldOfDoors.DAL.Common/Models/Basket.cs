﻿using System;
using System.Collections.Generic;
using WorldOfDoors.DAL.Common.Enum;
using WorldOfDoors.DAL.Common.Interfaces.Models;

namespace WorldOfDoors.DAL.Common.Models
{
    public class Basket : ModelBase
    {
        public int SetId { get; set; }

        public Set Set { get; set; }

        public int Quantity { get; set; }
        
        public IEnumerable<WriteOff> WriteOffs { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime ExpiredAt { get; set; }

        public BasketStatus Status { get; set; }
    }
}