﻿using WorldOfDoors.DAL.Common.Interfaces.Models;

namespace WorldOfDoors.DAL.Common.Models
{
    public class ContactInfo : ModelBase
    {
        public override string Name =>
            $"Contact information: {ZipCode}, {Region}, {District}, {City}, {Street} street, {Office}. Phone: {Phone}, fax{Fax}, Email: {Email}.";

        public string Phone { get; set; }

        public string Fax { get; set; }

        public string Email { get; set; }

        public string City { get; set; }

        public string Region { get; set; }

        public string District { get; set; }

        public string Street { get; set; }

        public string Office { get; set; }

        public string TimeZone { get; set; }

        public string ZipCode { get; set; }
    }
}