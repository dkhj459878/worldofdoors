﻿using System.Collections.Generic;
using WorldOfDoors.DAL.Common.Interfaces.Models;

namespace WorldOfDoors.DAL.Common.Models
{
    public class Invoice : DocumentBase
    {
        public List<Delivery> Deliveries { get; set; }

        public int SupplierId { get; set; }

        public Supplier Supplier { get; set; }
    }
}
