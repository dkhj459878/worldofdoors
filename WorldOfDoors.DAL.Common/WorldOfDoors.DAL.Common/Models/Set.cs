﻿using System.Collections.Generic;
using WorldOfDoors.DAL.Common.Interfaces.Models;

namespace WorldOfDoors.DAL.Common.Models
{
    public class Set : ModelBase
    {
        private IEnumerable<SetDetail> Details { get; set; }
    }
}