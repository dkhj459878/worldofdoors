﻿using WorldOfDoors.DAL.Common.Interfaces.Models;

namespace WorldOfDoors.DAL.Common.Models
{
    public class SetDetail : ModelBase
    {
        public int GoodId { get; set; }

        public Good Good { get; set; }

        public int Quantity { get; set; }
    }
}