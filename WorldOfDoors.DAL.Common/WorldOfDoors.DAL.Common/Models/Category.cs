﻿using System.Collections.Generic;
using WorldOfDoors.DAL.Common.Interfaces.Models;

namespace WorldOfDoors.DAL.Common.Models
{
    public class Category : ModelBase
    {
        public IEnumerable<Good> Goods { get; set; }
    }
}