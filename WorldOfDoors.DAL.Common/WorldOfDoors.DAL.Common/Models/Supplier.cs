﻿using WorldOfDoors.DAL.Common.Interfaces.Models;

namespace WorldOfDoors.DAL.Common.Models
{
    public class Supplier : ModelBase
    {
        public int ContactInfoId { get; set; }

        public ContactInfo ContactInfo { get; set; }
    }
}
