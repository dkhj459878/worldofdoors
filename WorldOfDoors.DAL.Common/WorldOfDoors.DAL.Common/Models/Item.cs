﻿using System.Collections.Generic;
using WorldOfDoors.DAL.Common.Interfaces.Models;

namespace WorldOfDoors.DAL.Common.Models
{
    public class Item : ModelBase
    {
        public override string Name =>
            $"Good {Good.Name} name in {Quantity} items with {Price} price and {Position} position.";

        public int Position { get; set; }

        public int OrderId { get; set; }

        public Order Order { get; set; }

        public int GoodId { get; set; }

        public Good Good { get; set; }

        public int Quantity { get; set; }

        public decimal Price { get; set; }

        public IEnumerable<WriteOff> WriteOffs { get; set; }
    }
}
