﻿using System;
using WorldOfDoors.DAL.Common.Interfaces.Models;

namespace WorldOfDoors.DAL.Common.Models
{
    public class Employee : ModelBase
    {
        public int SubjectId { get; set; }

        public Subject Subject { get; set; }

        public int PositionId { get; set; }

        public Position Position { get; set; }

        public DateTime HiredAt { get; set; }

        public DateTime FiredAt { get; set; }
    }
}