﻿using System;
using WorldOfDoors.DAL.Common.Interfaces.Models;

namespace WorldOfDoors.DAL.Common.Models
{
    public class Stock : ModelBase
    {
        public override string Name =>
            $"Good with '{Good.Name}' name in {Quantity} items with {Price} price and {Position} position. Posted to warehouse at {PostedToWarehouse}. Delivered in accordance with invoice № {Invoice.Number} at {Invoice.IssuedAt}.";

        public int Position { get; set; }

        public int InvoiceId { get; set; }

        public Invoice Invoice { get; set; }

        public int GoodId { get; set; }

        public Good Good { get; set; }

        public int Quantity { get; set; }

        public decimal Price { get; set; }

        public DateTime PostedToWarehouse { get; set; }
    }
}