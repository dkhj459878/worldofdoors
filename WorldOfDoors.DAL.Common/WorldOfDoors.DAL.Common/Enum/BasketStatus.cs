﻿namespace WorldOfDoors.DAL.Common.Enum
{
    public enum BasketStatus
    {
        None = 0,
        Created = 0,
        Expired = 1
    }
}