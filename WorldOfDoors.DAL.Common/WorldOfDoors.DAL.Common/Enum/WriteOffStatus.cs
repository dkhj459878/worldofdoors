﻿namespace WorldOfDoors.DAL.Common.Enum
{
    public enum WriteOffStatus
    {
        None = 0,
        Frozen = 1,
        Sold = 2
    }
}