﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Infrastructure;
using WorldOfDoors.DAL.Common.Models;

namespace WorldOfDoors.DAL.Common.Interfaces.DatabaseContexts
{
    public interface IDatabaseContext
    {
        DbSet<Basket> Baskets { get; set; }
        DbSet<Category> Categories { get; set; }
        DbSet<ContactInfo> ContactInfos { get; set; }
        DbSet<Delivery> Deliveries { get; set; }
        DbSet<Employee> Employees { get; set; }
        DbSet<Good> Goods { get; set; }
        DbSet<Invoice> Invoices { set; get; }
        DbSet<Item> Items { get; set; }
        DbSet<Order> Orders { get; set; }
        DbSet<Position> Positions { get; set; }
        DbSet<Set> Sets { get; set; }
        DbSet<SetDetail> SetDetails { get; set; }
        DbSet<Stock> Stocks { get; set; }
        DbSet<Subject> Subjects { get; set; }
        DbSet<SubjectType> SubjectTypes { get; set; }
        DbSet<Supplier> Suppliers { get; set; }
        DbSet<WriteOff> WriteOffs { get; set; }
        DatabaseFacade Database { get; }
        ChangeTracker ChangeTracker { get; }

        int SaveChanges();
    }
}