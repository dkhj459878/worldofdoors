﻿namespace WorldOfDoors.DAL.Common.Interfaces.Models
{
    public abstract class ModelBase
    {
        public int Id { get; set; }

        public virtual string Name { get; set; }
    }
}