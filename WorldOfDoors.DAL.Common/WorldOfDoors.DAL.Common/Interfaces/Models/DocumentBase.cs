﻿using System;

namespace WorldOfDoors.DAL.Common.Interfaces.Models
{
    public abstract class DocumentBase : ModelBase
    {
        public string Number { get; set; }

        public DateTime IssuedAt { get; set; }
    }
}