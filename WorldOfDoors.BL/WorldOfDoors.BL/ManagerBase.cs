﻿using WorldOfDoors.DAL.Common.Interfaces.DatabaseContexts;

namespace WorldOfDoors.BL
{
    public abstract class ManagerBase
    {
        protected readonly IDatabaseContext DatabaseContext;

        protected ManagerBase(IDatabaseContext databaseContext)
        {
            DatabaseContext = databaseContext;
        }
    }
}