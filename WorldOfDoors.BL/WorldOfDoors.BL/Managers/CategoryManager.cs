﻿using System.Linq;
using WorldOfDoors.BL.Common.Managers;
using WorldOfDoors.DAL.Common.Interfaces.DatabaseContexts;
using WorldOfDoors.DAL.Common.Models;

namespace WorldOfDoors.BL.Managers
{
    public class CategoryManager : ManagerBase, ICategoryManager
    {
        public Category GetCategory(int id)
        {
            return DatabaseContext.Categories.FirstOrDefault(c => c.Id == id);
        }

        public int AddCategory(Category category)
        {
            DatabaseContext.Categories.Add(category);
            DatabaseContext.SaveChanges();
            return category.Id;
        }

        public void UpdateCategory(Category category)
        {
            DatabaseContext.Categories.Update(category);
            DatabaseContext.SaveChanges();
        }

        public void DeleteCategory(Category category)
        {
            DatabaseContext.Categories.Update(category);
            DatabaseContext.SaveChanges();
        }

        public void DeleteCategory(int id)
        {
            var category = GetCategory(id);
            DeleteCategory(category);
        }

        public CategoryManager(IDatabaseContext databaseContext) : base(databaseContext)
        {
        }
    }
}