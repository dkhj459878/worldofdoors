﻿using System.Linq;
using WorldOfDoors.BL.Common.Managers;
using WorldOfDoors.DAL.Common.Interfaces.DatabaseContexts;
using WorldOfDoors.DAL.Common.Models;

namespace WorldOfDoors.BL.Managers
{
    public class ItemManager : ManagerBase, IItemManager
    {
        public Item GetItem(int id)
        {
            return DatabaseContext.Items.FirstOrDefault(c => c.Id == id);
        }

        public int AddItem(Item item)
        {
            DatabaseContext.Items.Add(item);
            DatabaseContext.SaveChanges();
            return item.Id;
        }

        public void UpdateItem(Item item)
        {
            DatabaseContext.Items.Update(item);
            DatabaseContext.SaveChanges();
        }

        public void DeleteItem(Item item)
        {
            DatabaseContext.Items.Update(item);
            DatabaseContext.SaveChanges();
        }

        public void DeleteItem(int id)
        {
            var item = GetItem(id);
            DeleteItem(item);
        }

        public ItemManager(IDatabaseContext databaseContext) : base(databaseContext)
        {
        }
    }
}