﻿using System.Linq;
using WorldOfDoors.BL.Common.Managers;
using WorldOfDoors.DAL.Common.Interfaces.DatabaseContexts;
using WorldOfDoors.DAL.Common.Models;

namespace WorldOfDoors.BL.Managers
{
    public class SupplierManager : ManagerBase, ISupplierManager
    {
        public Supplier GetSupplier(int id)
        {
            return DatabaseContext.Suppliers.FirstOrDefault(c => c.Id == id);
        }

        public int AddSupplier(Supplier supplier)
        {
            DatabaseContext.Suppliers.Add(supplier);
            DatabaseContext.SaveChanges();
            return supplier.Id;
        }

        public void UpdateSupplier(Supplier supplier)
        {
            DatabaseContext.Suppliers.Update(supplier);
            DatabaseContext.SaveChanges();
        }

        public void DeleteSupplier(Supplier supplier)
        {
            DatabaseContext.Suppliers.Update(supplier);
            DatabaseContext.SaveChanges();
        }

        public void DeleteSupplier(int id)
        {
            var supplier = GetSupplier(id);
            DeleteSupplier(supplier);
        }

        public SupplierManager(IDatabaseContext databaseContext) : base(databaseContext)
        {
        }
    }
}