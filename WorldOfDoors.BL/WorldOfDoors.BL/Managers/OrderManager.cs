﻿using System.Linq;
using WorldOfDoors.BL.Common.Managers;
using WorldOfDoors.DAL.Common.Interfaces.DatabaseContexts;
using WorldOfDoors.DAL.Common.Models;

namespace WorldOfDoors.BL.Managers
{
    public class OrderManager : ManagerBase, IOrderManager
    {
        public Order GetOrder(int id)
        {
            return DatabaseContext.Orders.FirstOrDefault(c => c.Id == id);
        }

        public int AddOrder(Order order)
        {
            DatabaseContext.Orders.Add(order);
            DatabaseContext.SaveChanges();
            return order.Id;
        }

        public void UpdateOrder(Order order)
        {
            DatabaseContext.Orders.Update(order);
            DatabaseContext.SaveChanges();
        }

        public void DeleteOrder(Order order)
        {
            DatabaseContext.Orders.Update(order);
            DatabaseContext.SaveChanges();
        }

        public void DeleteOrder(int id)
        {
            var order = GetOrder(id);
            DeleteOrder(order);
        }

        public OrderManager(IDatabaseContext databaseContext) : base(databaseContext)
        {
        }
    }
}