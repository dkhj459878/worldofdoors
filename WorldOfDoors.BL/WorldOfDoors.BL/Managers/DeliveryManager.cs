﻿using System.Linq;
using WorldOfDoors.BL.Common.Managers;
using WorldOfDoors.DAL.Common.Interfaces.DatabaseContexts;
using WorldOfDoors.DAL.Common.Models;

namespace WorldOfDoors.BL.Managers
{
    public class DeliveryManager : ManagerBase, IDeliveryManager
    {
        public Delivery GetDelivery(int id)
        {
            return DatabaseContext.Deliveries.FirstOrDefault(c => c.Id == id);
        }

        public int AddDelivery(Delivery delivery)
        {
            DatabaseContext.Deliveries.Add(delivery);
            DatabaseContext.SaveChanges();
            return delivery.Id;
        }

        public void UpdateDelivery(Delivery delivery)
        {
            DatabaseContext.Deliveries.Update(delivery);
            DatabaseContext.SaveChanges();
        }

        public void DeleteDelivery(Delivery delivery)
        {
            DatabaseContext.Deliveries.Update(delivery);
            DatabaseContext.SaveChanges();
        }

        public void DeleteDelivery(int id)
        {
            var delivery = GetDelivery(id);
            DeleteDelivery(delivery);
        }

        public DeliveryManager(IDatabaseContext databaseContext) : base(databaseContext)
        {
        }
    }
}