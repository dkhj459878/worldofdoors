﻿using System.Linq;
using WorldOfDoors.BL.Common.Managers;
using WorldOfDoors.DAL.Common.Interfaces.DatabaseContexts;
using WorldOfDoors.DAL.Common.Models;

namespace WorldOfDoors.BL.Managers
{
    public class GoodManager : ManagerBase, IGoodManager
    {
        public Good GetGood(int id)
        {
            return DatabaseContext.Goods.FirstOrDefault(c => c.Id == id);
        }

        public int AddGood(Good good)
        {
            DatabaseContext.Goods.Add(good);
            DatabaseContext.SaveChanges();
            return good.Id;
        }

        public void UpdateGood(Good good)
        {
            DatabaseContext.Goods.Update(good);
            DatabaseContext.SaveChanges();
        }

        public void DeleteGood(Good good)
        {
            DatabaseContext.Goods.Update(good);
            DatabaseContext.SaveChanges();
        }

        public void DeleteGood(int id)
        {
            var good = GetGood(id);
            DeleteGood(good);
        }

        public GoodManager(IDatabaseContext databaseContext) : base(databaseContext)
        {
        }
    }
}