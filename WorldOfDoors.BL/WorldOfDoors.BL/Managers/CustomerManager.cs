﻿using System.Linq;
using WorldOfDoors.BL.Common.Managers;
using WorldOfDoors.DAL.Common.Interfaces.DatabaseContexts;
using WorldOfDoors.DAL.Common.Models;

namespace WorldOfDoors.BL.Managers
{
    public class CustomerManager : ManagerBase, ICustomerManager
    {
        public Subject GetCustomer(int id)
        {
            return DatabaseContext.Subjects.FirstOrDefault(c => c.Id == id);
        }

        public int AddCustomer(Subject subject)
        {
            DatabaseContext.Subjects.Add(subject);
            DatabaseContext.SaveChanges();
            return subject.Id;
        }

        public void UpdateCustomer(Subject subject)
        {
            DatabaseContext.Subjects.Update(subject);
            DatabaseContext.SaveChanges();
        }

        public void DeleteCustomer(Subject subject)
        {
            DatabaseContext.Subjects.Update(subject);
            DatabaseContext.SaveChanges();
        }

        public void DeleteCustomer(int id)
        {
            var customer = GetCustomer(id);
            DeleteCustomer(customer);
        }

        public CustomerManager(IDatabaseContext databaseContext) : base(databaseContext)
        {
        }
    }
}