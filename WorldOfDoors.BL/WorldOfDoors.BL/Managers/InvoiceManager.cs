﻿using System.Linq;
using WorldOfDoors.BL.Common.Managers;
using WorldOfDoors.DAL.Common.Interfaces.DatabaseContexts;
using WorldOfDoors.DAL.Common.Models;

namespace WorldOfDoors.BL.Managers
{
    public class InvoiceManager : ManagerBase, IInvoiceManager
    {
        public Invoice GetInvoice(int id)
        {
            return DatabaseContext.Invoices.FirstOrDefault(c => c.Id == id);
        }

        public int AddInvoice(Invoice invoice)
        {
            DatabaseContext.Invoices.Add(invoice);
            DatabaseContext.SaveChanges();
            return invoice.Id;
        }

        public void UpdateInvoice(Invoice invoice)
        {
            DatabaseContext.Invoices.Update(invoice);
            DatabaseContext.SaveChanges();
        }

        public void DeleteInvoice(Invoice invoice)
        {
            DatabaseContext.Invoices.Update(invoice);
            DatabaseContext.SaveChanges();
        }

        public void DeleteInvoice(int id)
        {
            var invoice = GetInvoice(id);
            DeleteInvoice(invoice);
        }

        public InvoiceManager(IDatabaseContext databaseContext) : base(databaseContext)
        {
        }
    }
}